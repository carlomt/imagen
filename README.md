# imagen

Package to create images with rician noise.

to install:

`python setup.py install`

on linux maybe you want to install it only for your user

`python3 setup.py install --user`


## usage example:

`import numpy as np`

`from imagen.imagen import imagen`

`generator = imagen()`

set signal and variance values:

`generator.signal = 1000`

`generator.sigma = 700`

image dimension (all images will be squared):

`generator.imgsize = 512`

each region of signal will be generated starting from a seed with an average size blobsize:

`generator.blobsize = 20.`

`generator.nseeds = 3`

finally, each time you call the generate method it will yeld two images, one with only signal and the other with signal and noise:

`noised, signal = generator.generate()`

have a look of the file: test_imagen.ipynb