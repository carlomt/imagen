#!/usr/bin/env python
# coding: utf-8
from __future__ import print_function, division
import numpy as np
from scipy import ndimage

class imagen:
    def __init__(self):
        self.imgsize = 512

        self.signal = 1000
        self.sigma = 500

        self.blobsize = 10.
        self.nseeds = 10

    def generateAnImage(self):
        n = self.nseeds
        l = self.imgsize
        im = np.zeros((l, l))
        points = l*np.random.random((2, n**2))
        im[(points[0]).astype(np.int), (points[1]).astype(np.int)] = 1
        im = ndimage.gaussian_filter(im, sigma=l/(3.*n)*self.blobsize/10.)
        return im

    def generateBinaryImage(self):
        im = self.generateAnImage()
        mask = im > im.mean()
        newim = mask.astype(np.int16) * self.signal
        return newim 

    def generateRicianNumber(self, nu, sigma):
        x = np.random.normal(nu, sigma)
        y = np.random.normal(np.zeros_like(nu), sigma)    
        r = np.sqrt(np.multiply(x,x) + np.multiply(y,y))
        return r
    
    def addNoiseToImage(self, im, sigma):
        noised = self.generateRicianNumber(im, sigma)
        return noised

    def generate(self):
        sigma = self.sigma
        im = self.generateBinaryImage()
        noised = self.addNoiseToImage(im, sigma)
        return noised, im 

