from setuptools import setup,find_packages
import os, glob, re

packs=find_packages()
scripts_list=glob.glob('./bin/*.py')
install_req=[
    'scipy>=0.17.1',
    'numpy>=1.10.0'
]

# with open("README.md", "r") as fh:
#     long_description = fh.read()

PKG = "imagen"
VERSIONFILE = os.path.join(PKG, "__init__.py")
verstrline = open(VERSIONFILE, "rt").read()
VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
mo = re.search(VSRE, verstrline, re.M)
if mo:
    verstr = mo.group(1)
else:
    raise RuntimeError("Unable to find version string in %s." % (VERSIONFILE,))
    

setup(name=PKG,
      version=verstr,
      description='Package to create images with rician noise.',
      url='http://www.roma1.infn.it/~mancinit/',
      download_url = 'https://gitlab.com/carlomt/imagen/archive/'+verstr+'.tar.gz',
      author='Carlo Mancini Terracciano',
      author_email='carlo.mancini.terracciano@roma1.infn.it',
      license='MIT',
      scripts=scripts_list,
      install_requires=install_req,
      packages=packs,
      keywords = ['medical','image-analysis'],
      classifiers = ["Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
      ],
)
